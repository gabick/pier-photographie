var $j = jQuery.noConflict();
$j(function(){
	
	hideFilters();
	masonryGrid();
	
	function masonryGrid() {
		$j(document).bind('ready ajaxComplete', function () {
 			$j(".ds-masonry-portfolio .et_pb_portfolio_item").each(function () {
 				var link = $j(this).find('.et_pb_module_header a').attr('href');
 				$j(this).find(".et_pb_module_header, .post-meta").wrapAll('<a href='+link+' class="ds-portfolio-text"></a>');
 			});
		});
	}
	
	function hideFilters() {
		var filters = $j('.et_pb_portfolio_filters');
		var filter = filters.find('.et_pb_portfolio_filter');
			if (filter.length <=2) {
				filters.hide();
			}
	}

});

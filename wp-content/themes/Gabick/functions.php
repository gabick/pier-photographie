<?php
require_once( 'code/services-shortcode.php' );
require_once( 'code/added-social-icons.php' );
require_once( 'vendor/divi-disable-premade-layouts/divi-disable-premade-layouts.php' );
function my_enqueue_assets() {
    wp_enqueue_style( 'parent-main', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'nwayo-style-main', '/pub/build/styles/main.css' );
    wp_enqueue_style( 'nwayo-style-copyright', '/pub/build/styles/copyright.css' );
    wp_enqueue_style( 'nwayo-style-blog', '/pub/build/styles/blog.css' );
    wp_enqueue_style( 'nwayo-style-footer', '/pub/build/styles/footer.css' );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue_assets' );

function my_scripts_method() {
    wp_enqueue_script(
        'nwayo-dependencies',
         '/pub/build/scripts/dependencies-head-sync.js',
        array( 'jquery' ),
        true
    );
    wp_enqueue_script(
        'nwayo-dependencies-sync',
        '/pub/build/scripts/dependencies.js',
        array( 'jquery' ),
        true
    );
    wp_enqueue_script(
        'nwayo-main',
        '/pub/build/scripts/main.js',
        array( 'jquery' ),
        true
    );
    wp_enqueue_script(
        'masonry-grid',
        get_stylesheet_directory_uri() . '/js/custom-script.js',
        array( 'jquery' ),
        true
    );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_method', 15 );

/*================================================
#Load custom Blog Module
================================================*/

//function divi_child_theme_setup() {
//    if ( ! class_exists('ET_Builder_Module') ) {
//        return;
//    }
//
//    get_template_part( 'custom-modules//Blog' );
//    $gabickBlog = new Gabick_ET_Builder_Module_Blog();
//    remove_shortcode( 'et_pb_portfolio' );
//    add_shortcode( 'et_pb_portfolio', array($gabickBlog, '_shortcode_callback') );
//
//}
//
//add_action( 'wp', 'divi_child_theme_setup', 9999 );

?>

var $j = jQuery.noConflict();
const URL = location.host;
const CATEGORY_SLUG = 'category'
$j(function(){

    footerCopyright();
	masonryGrid();

    function returnCategoryTitle() {
        var pathName = location.pathname;
        if (pathName.indexOf(CATEGORY_SLUG) >= 0) {
            var titleArray = pathName.split('/');
            // -2 because of index 0 of array
            var title = titleArray[titleArray.length-2];
            var title = title.replace(/-/g, " ");
            var stringToAdd = '<h1 style="text-align: center;">'+title+'</h1>'
            var element = $j('.heading-category .et_pb_text_inner').html(stringToAdd);

        }
    }

    function footerCopyright() {
        var $footer = $j("#footer-info")
        var $date = new Date().getFullYear();
        $footer.append('<span class="copyright">'+'\u00A9'+' '+$date+'</span>')
    }
	
	function masonryGrid() {
		$j(document).bind('ready ajaxComplete', function () {
 			$j(".ds-masonry-portfolio .et_pb_portfolio_item").each(function () {
 				$j(this).find(".et_pb_module_header, .post-meta").wrapAll('<div class="ds-portfolio-text"></div>');
 			});
		});
	}

});
<?php
//avoid direct calls to this file
if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

/**
 * Adds WP_Editor_Widget widget.
 */
class WP_Editor_Widget extends WP_Widget {

    var $textdomain;

    /**
	 * Register widget with WordPress.
	 */
	public function __construct() {
        $this->textdomain = 'gabick-promobar';
		// WPML support?
		if ( function_exists( 'icl_get_languages' ) ) {
			$widget_name = esc_html__( 'Multilingual', $this->textdomain ) . ' ' . esc_html__( 'Rich text', $this->textdomain );
		}
		else {
			$widget_name = esc_html__( 'Rich text', $this->textdomain );
		}

		$widget_ops = apply_filters(
			'gabick_promobar_ops',
			array(
				'classname' 	=> 'WP_Editor_Widget',
				'description' 	=> __( 'Arbitrary text, HTML or rich text through the standard WordPress visual editor.', $this->textdomain ),
			)
		);

		parent::__construct(
			'WP_Editor_Widget',
			$widget_name,
			$widget_ops
		);

	} // END __construct()

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

		$title			= apply_filters( 'gabick_promobar_title', $instance['title'] );
		$output_title	= apply_filters( 'gabick_promobar_output_title', $instance['output_title'] );
		$content		= apply_filters( 'gabick_promobar_content', $instance['content'] );

		$show = true;

		// WPML support?
		if ( function_exists( 'icl_get_languages' ) ) {
			$language = apply_filters( 'gabick_promobar_language', $instance['language'] );
			$show = ($language == icl_get_current_language());
		}

		if ( $show ) {

			$default_html = $args['before_widget'];

			if ( '1' == $output_title && ! empty( $title ) ) {
				$default_html .= $args['before_title'] . $title . $args['after_title'];
			}

			$default_html .= $content;

			$default_html .= $args['after_widget'];

			echo apply_filters( 'gabick_promobar_html', $default_html, $args['id'], $instance, $args['before_widget'], $args['after_widget'], $output_title, $title, $args['before_title'], $args['after_title'], $content );

		}

	} // END widget()

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		if ( isset( $instance['content'] ) ) {
			$content = $instance['content'];
		}
		else {
			$content = '';
		}

        if ( isset( $instance['text_color'] ) ) {
            $textColor = $instance['text_color'];
        }
        else {
            $textColor = '#ffffff';
        }

        if ( isset( $instance['background_color'] ) ) {
            $backgroundColor = $instance['background_color'];
        }
        else {
            $backgroundColor = '#000000';
        }

        ?>
        <script type='text/javascript'>
			jQuery(document).ready(function($) {
				$('.gabick-color-picker-text').wpColorPicker();
				$('.gabick-color-picker-background').wpColorPicker();
			});
        </script>
        <p>
            <label for="<?php echo $this->get_field_id( 'text_color' ); ?>"><?php _e( 'Text Color', $this->textdomain ); ?></label>
            <input class="gabick-color-picker-text" type="text" id="<?php echo $this->get_field_id( 'text_color' ); ?>" name="<?php echo $this->get_field_name( 'text_color' ); ?>" value="<?php echo esc_attr( $textColor ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'background_color' ); ?>"><?php _e( 'Background Color', $this->textdomain ); ?></label>
            <input class="gabick-color-picker-background" type="text" id="<?php echo $this->get_field_id( 'background_color' ); ?>" name="<?php echo $this->get_field_name( 'background_color' ); ?>" value="<?php echo esc_attr( $backgroundColor ); ?>" />
        </p>
		<input type="hidden" id="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'content' ) ); ?>" value="<?php echo esc_attr( $content ); ?>">
		<p>
			<a href="javascript:GabickPromoBar.showEditor('<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>');" class="button"><?php _e( 'Edit content', $this->textdomain ) ?></a>
		</p>

        <?php // WPML support ?>
        <?php if ( function_exists( 'icl_get_languages' ) ) : $languages = icl_get_languages( 'skip_missing=0&orderby=code' ); ?>
            <label for="<?php echo esc_attr( $this->get_field_id( 'language' ) ); ?>">
                <?php _e( 'Language', 'wp-editor-widget' ); ?>:
                <select id="<?php echo esc_attr( $this->get_field_id( 'language' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'language' ) ); ?>">
                    <?php foreach ( $languages as $id => $lang ) : ?>
                        <option value="<?php echo esc_attr( $lang['language_code'] ) ?>" <?php selected( $instance['language'], $lang['language_code'] ) ?>><?php echo esc_attr( $lang['native_name'] ) ?></option>
                    <?php endforeach; ?>
                </select>
            </label>
        <?php endif; ?>
		<?php
        do_action( 'gabick_promobar_form', $this, $instance );

	} // END form()

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = array();

		$instance['content']		= ( ! empty( $new_instance['content'] ) ? $new_instance['content'] : '' );
		$instance['text_color']		= ( ! empty( $new_instance['text_color'] ) ? $new_instance['text_color'] : '' );
		$instance['background_color']		= ( ! empty( $new_instance['background_color'] ) ? $new_instance['background_color'] : '' );

		// WPML support
		if ( function_exists( 'icl_get_languages' )  ) {
			$instance['language']   = ( isset( $new_instance['language'] ) ? $new_instance['language'] : '');
		}

		do_action( 'gabick_promobar_update', $new_instance, $instance );

 	 	return apply_filters( 'gabick_promobar_update_instance', $instance, $new_instance );

	} // END update()

} // END class WP_Editor_Widget
function gabickTest() {
    if ( function_exists('register_sidebar') ) {
        register_sidebar(array(
                'name' => 'Promo bar Area',
                'before_widget' => '<div class = "promo-banner-area">',
                'after_widget' => '</div>',
                'id' => 'promo-bar-area'
            )
        );
    }
}
add_action( 'genesis_before_header', 'gabickTest' );


<?php
/*
Plugin Name: Gabick Promo Bar
Plugin URI: https://github.com/feedmeastraycat/gabick-promobar
Description: Gabick Promo Bar adds a WYSIWYG widget using the wp_editor().
Author: David M&aring;rtensson
Version: 0.6.0
Author URI: https://github.com/feedmeastraycat/gabick-promobar
Text Domain: gabick-promobar
Domain Path: /langs
*/

//avoid direct calls to this file
if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

include 'classes/class-widget.php';

/**
 * Gabick Promo Bar singelton
 */
class GabickPromoBar {

	/**
	 * @var string
	 */
	const VERSION = '1.0.0';

	/**
	 * Action: init
	 */
	public function __construct() {

		add_action( 'widgets_init', array( $this, 'widgets_init' ) );
		add_action( 'load-widgets.php', array( $this, 'load_admin_assets' ) );
		add_action( 'load-customize.php', array( $this, 'load_admin_assets' ) );
		add_action( 'widgets_admin_page', array( $this, 'output_gabick_promobar_html' ), 100 );
		add_action( 'customize_controls_print_footer_scripts', array( $this, 'output_gabick_promobar_html' ), 1 );
		add_action( 'customize_controls_print_footer_scripts', array( $this, 'customize_controls_print_footer_scripts' ), 2 );
		add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );

		add_filter( 'gabick_promobar_content', 'wptexturize' );
		add_filter( 'gabick_promobar_content', 'convert_smilies' );
		add_filter( 'gabick_promobar_content', 'convert_chars' );
		add_filter( 'gabick_promobar_content', 'wpautop' );
		add_filter( 'gabick_promobar_content', 'shortcode_unautop' );
		add_filter( 'gabick_promobar_content', 'do_shortcode', 11 );

	} // END __construct()

	/**
	 * Action: load-widgets.php
	 * Action: load-customize.php
	 */
	public function load_admin_assets() {

		wp_register_script( 'gabick-promobar-js', plugins_url( 'assets/js/admin.js', __FILE__ ), array( 'jquery' ), self::VERSION );
		wp_enqueue_script( 'gabick-promobar-js' );

		wp_register_style( 'gabick-promobar-css', plugins_url( 'assets/css/admin.css', __FILE__ ), array(), self::VERSION );
		wp_enqueue_style( 'gabick-promobar-css' );

	} // END load_admin_assets()

    public function load_admin_color_picker() {
        wp_enqueue_style( 'gabick-color-picker' );
        wp_enqueue_script( 'gabick-color-picker' );
    }

	/**
	 * Action: plugins_loaded
	 */
	public function plugins_loaded() {

		// Load translations
		load_plugin_textdomain( 'gabick-promobar', false, dirname( plugin_basename( __FILE__ ) ) . '/langs/' );

	} // END plugins_loaded()

	/**
	 * Action: widgets_admin_page
	 * Action: customize_controls_print_footer_scripts
	 */
	public function output_gabick_promobar_html() {

		?>
		<div id="gabick-promobar-container" style="display: none;">
			<a class="close" href="javascript:GabickPromoBar.hideEditor();" title="<?php esc_attr_e( 'Close', 'gabick-promobar' ); ?>"><span class="icon"></span></a>
			<div class="editor">
				<?php
				$settings = array(
					'textarea_rows' => 20,
				);
				wp_editor( '', 'wpeditorwidget', $settings );
				?>
				<p>
					<a href="javascript:GabickPromoBar.updateWidgetAndCloseEditor(true);" class="button button-primary"><?php _e( 'Save and close', 'gabick-promobar' ); ?></a>
				</p>
			</div>
		</div>
		<div id="gabick-promobar-backdrop" style="display: none;"></div>
		<?php

	} // END output_gabick_promobar_html

	/**
	 * Action: customize_controls_print_footer_scripts
	 */
	public function customize_controls_print_footer_scripts() {

		// Because of https://core.trac.wordpress.org/ticket/27853
		// Which was fixed in 3.9.1 so we only need this on earlier versions
		$wp_version = get_bloginfo( 'version' );
		if ( version_compare( $wp_version, '3.9.1', '<' ) && class_exists( '_WP_Editors' ) ) {
			_WP_Editors::enqueue_scripts();
		}

	} // END customize_controls_print_footer_scripts

	/**
	 * Action: widgets_init
	 */
	public function widgets_init() {

		register_widget( 'WP_Editor_Widget' );

	} // END widgets_init()

} // END class GabickPromoBar

global $gabick_promobar;
$gabick_promobar = new GabickPromoBar;

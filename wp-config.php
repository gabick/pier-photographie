<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pierphotographie_database' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

//define('WP_HTTP_BLOCK_EXTERNAL', true);
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&[!9 g$|sq~DXn{KQW3^@7mr41`^?uINoJ@6.vMl+pNJaf|.:-lOT>=cqRuS#K<R' );
define( 'SECURE_AUTH_KEY',  '=v HtoZ}(<PFyLGR^8WLI;`C,c 8:WD)R({uDt6ED@R-zoHC?mg+Nr~,-fb(l6R9' );
define( 'LOGGED_IN_KEY',    '<fXU{hg@J1&]L3&pCO_%manj,A3}.A9*r!JMa@9`irs-DdYmQSu]bE9fWK<`,m6c' );
define( 'NONCE_KEY',        '|8INRpv6uZQ!0@2>v>b|^eh%#Y=Ej:9^l.0cxO#=yW1zw:+L7T;zHGI/d-__t?25' );
define( 'AUTH_SALT',        'O|53R5#dJUHI0TV^^oNyvmJN}][sJDL%G)+$.{|Bh6IsBemr8x}u$a2,qL<zJBt ' );
define( 'SECURE_AUTH_SALT', 'YE?jSdU[g7`(tL[e%TI4o-E|RQO-jJ}Ive~Ui#R#^Lu[a0]UN_9E3e7*C=Dv*e%u' );
define( 'LOGGED_IN_SALT',   '?3IQ7XtQ8,s}wu14UF9[Dz5!whLB6?S@H{3ncFLIR,Y.5Uvm]n%^q_x9c.U)5$#2' );
define( 'NONCE_SALT',       '6`V_!r;*Ft%<=:WxKu8rXM_M@3ag:t>[:NvfI8)E)O{bOMV+Uil/9al@]vKcj6R<' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
